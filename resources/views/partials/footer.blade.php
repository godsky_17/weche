<footer class="footer">
    <div class="footer__main">
        <div class="container">
            <div class="footer__logo">
                <img src="dist/img/logo-mtfp-white.png" alt="MTFP">
            </div>
            <!-- .footer__logo -->
            <h5 class="footer__desc text-white text-center">Minist&egrave;re du Travail et de la Fonction Publique
            </h5>
            <ul class="footer__social">

            </ul>
            <!-- .footer__social -->
        </div>
        <!-- .container -->

    </div>
    <!-- .footer__main -->

    <div class="footer__copyright">
        <div class="container">
            <div class="footer__copyright-inner">
                <p class="footer__copyright-desc">
                    2023 &copy; Minist&egrave;re du travail et de la fonction publique. Tout droits
                    r&eacute;serv&eacute;s.
                </p>
                <ul class="footer__copyright-list">
                    <li><a href="contact">Nous contacter</a></li>
                    <li><a href="https://travail.gouv.bj/">Site web du minist&egrave;re</a></li>
                    <li><a href="https://demarchesmtfp.gouv.bj/">Portail des d&eacute;marches administratives</a>
                    </li>
                </ul>
            </div><!-- .footer__copyright-inner -->
        </div><!-- .container -->
    </div><!-- .footer__copyright -->
    <span class="flag flex f-row w100 flex-1"><i></i><i></i><i></i></span>
</footer><!-- .footer --><!-- END FOOTER HERE ---><!-- begin::Scroll Top --><a href="#" class="back-to-top"><span class="fa fa-arrow-up"></span></a>
<script src="dist/js/script.js"></script>
<script src="js/wizadForm.js"></script>
<script src="dist/bootstrap/js/bootstrap.min.js"></script>
<script type="text&#x2F;javascript" src="dist/js/login/index.js"></script>
<script type="text&#x2F;javascript" src="dist/custom_plugins/jquery.min.js"></script>
<script type="text&#x2F;javascript" src="dist/custom_plugins/jquery-migrate.min.js"></script>
<script type="text&#x2F;javascript" src="dist/realand/js/plugins.js"></script>
<script type="text&#x2F;javascript" src="dist/custom_plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<script type="text&#x2F;javascript" src="dist/custom_plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text&#x2F;javascript" src="dist/custom_plugins/jquery-validation/js/additional-methods.min.js"></script>
<script type="text&#x2F;javascript" src="dist/custom_plugins/jquery-validation/js/localization/messages_fr.min.js"></script>
<script type="text&#x2F;javascript" src="dist/custom_plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text&#x2F;javascript" src="dist/custom_plugins/select2_alt/js/select2.full.min.js"></script>
<script type="text&#x2F;javascript" src="dist/realand/js/custom.js"></script>
<script>
    jQuery(document).ready(function() {
        $('.reset-form').on('click', function() {
            $(this).closest('form').find('input[type=text], textarea, select').val('');
        });
    });
</script>