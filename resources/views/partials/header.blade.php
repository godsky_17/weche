<!-- .page-loader -->
    <!-- begin:: Page -->
    <header class="header header--blue">
        <div class="header__overlay"></div>
        <div class="container">
            <div class="header__main">
                <div class="header__logo">
                    <a href="/">
                        <h1 class="screen-reader-text">Weche</h1>
                        <img src="dist/img/logo-mtfp-benin.png" alt="Realand">
                    </a>
                </div>
                <!-- .header__main -->
                <div class="nav-mobile">
                    <a href="#" class="nav-toggle">
                        <span></span>
                    </a>
                    <!-- .nav-toggle -->
                </div>
                <!-- .nav-mobile -->
                <div class="header__menu header__menu--v1">
                    <ul class="header__nav">
                        <li class="header__nav-item ">
                            <a href="{{ route('faq')}}" class="header__nav-link ">Aide</a>
                        </li>
                        <li class="header__nav-item ">
                            <a href="contact" class="header__nav-link ">Nous contacter</a>
                        </li>
                    </ul>
                    <!-- .header__nav -->
                </div>
                <!-- .header__menu -->
                <!-- .header__menu -->
                <!-- begin::Header -->
                <a href="{{ route('contact')}}" class="header__cta"><i class="fa fa-envelope"></i> Allo retraite</a>
                <!-- end::Header -->
            </div>
            <!-- .header__main -->
        </div>
        <!-- .container -->
    </header>
    <!-- .header -->