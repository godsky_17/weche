@extends('base')
@section('container')
<div class="row containt mv-50">
    <div class="col-md-5 left ">
        <div class="row user-name container-mine">
            <div class="overlay">
                <p>{{ Auth::user()->categorie }}</p>
            </div>
            <h2 class="name"><span class="uppercase">{{ Auth::user()->nom }}</span> {{ Auth::user()->prenom }}</h2>
            <span class="matricule">{{ Auth::user()->matricule }}</span>
        </div>
        <div class="row container-mine">
            @if (!empty(Auth::user()->telephone) OR !empty(Auth::user()->email) )
            <div class="row info-box">
                <h4 class="title">Contact</h4>
                @if (!empty(Auth::user()->email))
                <p class="user-info"><span></span>{{ Auth::user()->email }}</p>
                @endif
                @if (!empty(Auth::user()->telephone))
                <p class="user-info">
                    <span></span>
                    {{Auth::user()->telephone}}
                </p>
                @endif

            </div>
            @endif

            <div class="row">
                @if (!empty($diplome))
                <div class="col-2 info-box">
                    <h4 class="title">Diplome(s)</h4>
                    <p class="user-info uppercase"><span></span>{{ $diplome }}</p>
                </div>
                @endif
                @if (!empty($distinction))
                <div class="col-3 info-box">
                    <h4 class="title">Distinction(s)</h4>
                    <p class="user-info"><span></span> {{ $distinction }}</p>
                </div>
                @endif


            </div>
            <div class="row info-box">
                @if (!empty($pension))
                <h4 class="title">Régime de retraite : <span class="user-info uppercase">{{ $pension }}</span></h4>
                @endif
                <h4 class="title">Année de service : <span class="user-info uppercase">5</span></h4>
                <h4 class="title">Service militaire : <span class="user-info uppercase">0</span></h4>
            </div>
        </div>

    </div>
    <div class="col-md-7 right container-mine">
        <div class="card-area">
            <div class="card"></div>
            <div class="card"></div>
            <div class="card"></div>
        </div>
        <div class="row menu">
            <a href="#">
                <div class="menu-item">
                    <div class="text">
                        <p>Générer une fiche de note</p>
                    </div>
                    <div class="img"><img src="img/93e12074377e36168b21a6ebc2d04cd5.jpg" alt="" srcset=""></div>
                </div>
            </a>
            <a href="{{ route('fonctionnaire.resultat')}}">
                <div class="menu-item">
                    <div class="text">
                        <p>Mes performances</p>
                    </div>
                    <div class="img"><img src="img/93e12074377e36168b21a6ebc2d04cd5.jpg" alt="efeez" srcset=""></div>
                </div>
            </a>
            <a href="{{ route('fonctionnaire.parametre')}}">
                <div class="menu-item">
                    <div class="text">
                        <p>Modifier mes informations</p>
                    </div>
                    <div class="img"><img src="img/93e12074377e36168b21a6ebc2d04cd5.jpg" alt="hello" srcset="" class="menu_img"></div>
                </div>
            </a>
        </div>
    </div>
</div>
@if ((Auth::user()->wizard) == 0)
<script>
    window.location.href = 'http://localhost:8000/bienvenue';
</script>
@endif
@endsection