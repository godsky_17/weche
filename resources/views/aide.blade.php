@extends('base')
@section('container')

<div class="row">
                <main class="col-md-12">
                    <div class="property__feature-container">
                        <div class="property__feature">
                            <h3 class="property__feature-title property__feature-title--b-spacing">F.A.Q</h3>
                            <div class="property__accordion">
                                <div class="property__accordion-header">
                                    <div class="property__accordion-textcontent">
                                        <span class="property__accordion-title">Comment s'inscrire sur la plateforme?</span>
                                    </div><!-- .property__accordion-textcontent -->
                                    <i class="fa fa-caret-up property__accordion-expand" aria-hidden="true"></i>
                                </div><!-- .property__accordion-header -->
                                <div class="property__accordion-content property__accordion-content--active">
                                    <p class="">Cliquez sur le menu <a href="/login">S'authentifier</a> et ensuite sur l'onglet <b>"S'inscrire"</b>. Remplissez ensuite
                                    le formulaire d'inscription en renseignant les informations suivantes: <b>matricule, nom (suivi de EPSE... pour le femme déclarées mariées),
                                        prénom(s), date de naissance, numéro de téléphone, email et mot de passe</b>. Assurez-vous que votre adresse email soit bien renseigné
                                    et valide car il vous servira à activer votre compte.</p>
                                    <p>Après inscription, vous recevrez un email d'activation de compte. Consultez votre boîte de réception puis cliquez sur le lien
                                    envoyé et votre compte sera activé. Vous pourrez ensuite vous authentifier pour accéder à votre espace carrière.</p>
                                    <p><b>PS: Le lien de validation expire après 48h et vous serez obligé de reprendre votre inscription après expiration du lien.</b></p>
                                </div><!-- .property__accordion-content -->
                            </div><!-- .property__accordion -->
                            <div class="property__accordion">
                                <div class="property__accordion-header">
                                    <div class="property__accordion-textcontent">
                                        <span class="property__accordion-title">Comment s'authentifier sur la plateforme?</span>
                                    </div><!-- .property__accordion-textcontent -->
                                    <i class="fa fa-caret-down property__accordion-expand" aria-hidden="true"></i>
                                </div><!-- .property__accordion-header -->
                                <div class="property__accordion-content">
                                    <p>Pour vous authentifier, cliquez sur le menu <a href="/login">S'authentifier</a> si vous n'êtes pas encore sur la page d'authentification.
                                        Renseignez ensuite votre <b>identifiant (email ou matricule)</b> et <b>votre mot de passe</b> puis cliquez sur le bouton "Se connecter".</p>
                                </div><!-- .property__accordion-content -->
                            </div><!-- .property__accordion -->
                            <div class="property__accordion">
                                <div class="property__accordion-header">
                                    <div class="property__accordion-textcontent">
                                        <span class="property__accordion-title">Comment récupérer son mot de passe?</span>
                                    </div><!-- .property__accordion-textcontent -->
                                    <i class="fa fa-caret-down property__accordion-expand" aria-hidden="true"></i>
                                </div><!-- .property__accordion-header -->
                                <div class="property__accordion-content">
                                    <p>Au cas où vous avez oublié votre mot de passe, pas de panique. Allez sur la page d'authentification et cliquez sur le lien
                                        <a href="/login/pass-reset">Mot de passe oublié</a>. Sur la page de récupération de mot de passe, vous allez renseigner votre
                                    <b>matricule</b> et votre <b>adresse email</b> que vous avez utilisé pour vous inscrire.</p>
                                    <p>Vous recevrez par la suite un mail de réinitialisation de mot de passe contenant un lien. Cliquez sur le lien envoyé et vous
                                    arriverai sur une nouvelle page où vous allez définir un nouveau mot de passe.</p>
                                </div><!-- .property__accordion-content -->
                            </div><!-- .property__accordion -->
                            <div class="property__accordion">
                                <div class="property__accordion-header">
                                    <div class="property__accordion-textcontent">
                                        <span class="property__accordion-title">Comment télécharger ses actes?</span>
                                    </div><!-- .property__accordion-textcontent -->
                                    <i class="fa fa-caret-down property__accordion-expand" aria-hidden="true"></i>
                                </div><!-- .property__accordion-header -->
                                <div class="property__accordion-content">
                                    <p>Les actes de carrière suivent un processus de numérisation avant d'être mis en ligne. Une fois que les actes
                                    sont en ligne et que vous êtes connecté à votre espace carrière; vous verrez pour vos actes disponibles en fichier
                                    un bouton <b>Visualiser</b> qui vous permet de consulter l'acte en ligne et un bouton <b>Télécharger</b> qui vous permet
                                    de télécharger votre acte.</p>
                                </div><!-- .property__accordion-content -->
                            </div><!-- .property__accordion -->
                            <div class="property__accordion">
                                <div class="property__accordion-header">
                                    <div class="property__accordion-textcontent">
                                        <span class="property__accordion-title">Comment faire une demande de correction de ses informations de carrière?</span>
                                    </div><!-- .property__accordion-textcontent -->
                                    <i class="fa fa-caret-down property__accordion-expand" aria-hidden="true"></i>
                                </div><!-- .property__accordion-header -->
                                <div class="property__accordion-content">
                                    <p>Pour faire une demande de correction de vos informations de carrière (nom mal écrit par exemple), veuillez adresser un courrier
                                    au <b>Ministre du Travail et de la Fonction Publique, attention Directeur(trice) Général(e) de la Fonction Publique (DGFP)</b> en précisant
                                    les informations à corriger et en y joignant les pièces justificatives.</p>
                                </div><!-- .property__accordion-content -->
                            </div><!-- .property__accordion -->
                            <div class="property__accordion">
                                <div class="property__accordion-header">
                                    <div class="property__accordion-textcontent">
                                        <span class="property__accordion-title">Comment imprimer ma fiche de paie?</span>
                                    </div><!-- .property__accordion-textcontent -->
                                    <i class="fa fa-caret-down property__accordion-expand" aria-hidden="true"></i>
                                </div><!-- .property__accordion-header -->
                                <div class="property__accordion-content">
                                    <p>La plateforme de téléchargement et d'impression des fiches de paie est une plateforme du trésor. <a href="https://www.bulletinpaie.finances.bj:8443/" target="_blank">Cliquez ici</a> pour y accéder.</p>
                                </div><!-- .property__accordion-content -->
                            </div><!-- .property__accordion -->
                            <div class="property__accordion">
                                <div class="property__accordion-header">
                                    <div class="property__accordion-textcontent">
                                        <span class="property__accordion-title">Comment suivre le traitement de mon acte en cours?</span>
                                    </div><!-- .property__accordion-textcontent -->
                                    <i class="fa fa-caret-down property__accordion-expand" aria-hidden="true"></i>
                                </div><!-- .property__accordion-header -->
                                <div class="property__accordion-content">
                                     <p>Le suivi du traitement des actes est géré par la DAF de chaque ministère. Rapprochez-vous donc de la DAF de votre structure
                                     dans un premier temps pour vous renseigner sur l'évolution de votre dossier. En cas de non satisfaction, vous pouvez vous rapprocher
                                     du <b>Service de Relation avec les Usagers (SRU)</b> du Ministère du Travail et de la Fonction Publique.</p>
                                </div><!-- .property__accordion-content -->
                            </div><!-- .property__accordion -->
                        </div><!-- .property__feature -->
                    </div><!-- .property__feature-container -->
                </main>
            </div><!-- .row -->

@endsection