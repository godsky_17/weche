@extends('base')
@section('container')
<div class="contact__main">
    <div class="row">
        <div class="col-md-12">
        </div>
        <div class="col-md-6">
            <h2 class="contact__title text--dark-blue">Nous envoyer un message</h2>
            <div class="contact__desc">
                <p>Vous pouvez nous envoyer vos questions ou demandes d'informations au cas où vous n'avez pas trouvé de reponse à votre
                    préocupation dans notre <a href="{{ route('faq') }}">Foire à question (F.A.Q.)</a></p>
            </div>
            <form class="contact-form contact-form--no-padding" method="post" action="/home/contact">
                <div class="contact-form__body">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <input type="text" class="contact-form__field contact-form__field--contact" placeholder="Matricule" name="matricule" required>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <input type="text" class="contact-form__field contact-form__field--contact" placeholder="Nom" name="nom" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <input type="tel" class="contact-form__field contact-form__field--contact" placeholder="Téléphone" name="phone" required>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <input type="email" class="contact-form__field contact-form__field--contact" placeholder="Email" name="email" required>
                        </div>
                    </div>
                    <input type="text" class="contact-form__field contact-form__field--contact" placeholder="Objet" name="subject" required>
                    <textarea class="contact-form__field contact-form__comment contact-form__field--contact" cols="30" rows="4" placeholder="Message" name="message" required></textarea>

                </div><!-- .contact-form__body -->
                <div class="contact-form__footer">
                    <input type="submit" class="contact-form__submit contact-form__submit--contact" name="submit" value="Envoyer">
                </div><!-- .contact-form__footer -->
            </form><!-- .contact-form -->
        </div><!-- .col -->
        <div class="col-md-5 col-md-offset-1 col-xs-12">
            <h2 class="contact__title text--dark-blue">Nous Contacter</h2>
            <div class="contact__desc">
                <p>Vous pouvez aussi nous appeler ou vous rendre au service de relation avec les usagers du Ministère du Travail et de la Fonction Publique</p>
                <p>Du <strong>Lundi</strong> au <strong>Vendredi, de 9:00 à 11:30 et de 16:00 à 17:30</strong></p>
            </div>
            <ul class="agency__contact">
                <li class="agency__contact-phone"><a href="tel+22921332330">(+229) 21 33 23 30</a></li>
                <li class="agency__contact-website"><a href="https://travail.gouv.bj/">https://travail.gouv.bj</a></li>
                <li class="agency__contact-email"><a href="mailto:contact-weche@fpbenin.net">contact-weche@fpbenin.net</a></li>
                <li class="agency__contact-address">Tour administrative B, Cadjèjoun, Cotonou</li>
            </ul>
        </div><!-- .col -->
    </div><!-- .row -->
</div>
@endsection