<!DOCTYPE html>
<html lang="fr"><!-- BEGIN HEAD -->
<!-- Mirrored from weche.fpbenin.net/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 19 Jun 2023 12:20:37 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->

<head>
    <meta charset="utf-8" />
    <title>Weche</title>
    <meta name="viewport" content="width&#x3D;device-width,&#x20;user-scalable&#x3D;no,&#x20;initial-scale&#x3D;1.0,&#x20;minimum-scale&#x3D;1.0,&#x20;maximum-scale&#x3D;1.0">
    <meta name="description" content="">
    <meta name="author" content="Gbemiga&#x20;OGOUBY">
    <meta name="keywords" content="">
    <meta name="format-detection" content="telephone-no"> <!-- Mobile specific meta -->
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="dist/front_assets/webfont/webfont.html"></script>
    <script>
        WebFont.load({
            google: {
                "families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
            },
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!-- BEGIN HEAD CSS -->
    <link href="dist/img/fav.png" rel="shortcut&#x20;icon">
    <link href="https://fonts.googleapis.com/css?family=Open Sans:400,300,600,700&amp;subset=all" media="screen" rel="stylesheet" type="text&#x2F;css">

    
    <link href="dist/realand/css/plugins.css" media="screen" rel="stylesheet" type="text&#x2F;css">
    <link href="dist/realand/css/style.css" media="screen" rel="stylesheet" type="text&#x2F;css">
    <link href="dist/metro_2/simple-line-icons/simple-line-icons.min.css" media="screen" rel="stylesheet" type="text&#x2F;css">
    <link href="dist/custom_plugins/font-awesome/css/font-awesome.min.css" media="screen" rel="stylesheet" type="text&#x2F;css">
    <link href="dist/custom_plugins/bootstrap-sweetalert/sweet-alert.css" media="screen" rel="stylesheet" type="text&#x2F;css">
    <link href="dist/custom_plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" media="screen" rel="stylesheet" type="text&#x2F;css">
    <link href="dist/custom_plugins/select2_alt/css/select2.css" media="screen" rel="stylesheet" type="text&#x2F;css">
    <link href="dist/custom_plugins/select2_alt/css/select2-bootstrap.min.css" media="screen" rel="stylesheet" type="text&#x2F;css">
    <link href="dist/css/custom.css" media="screen" rel="stylesheet" type="text&#x2F;css"> 
    <link rel="stylesheet" href="dist/css/style.css"><!-- END HEAD CSS-->
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <!--   <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>       <!-- END GLOBAL MANDATORY STYLES -->
    <script type="text&#x2F;javascript" src="dist/js/login/index.js"></script>
    <script type="text&#x2F;javascript" src="dist/custom_plugins/jquery.min.js"></script>
    <script type="text&#x2F;javascript" src="dist/custom_plugins/jquery-migrate.min.js"></script>
</head>
<!-- BEGIN HEAD -->
<!-- BEGIN BODY -->
<!-- BEGIN BODY -->
<!--oncontextmenu="return false"-->

<body class="">
    @include('partials.laoder')
    @include('partials.header')
    <!-- .header -->
    <section class="sign-up">
        <div class="container">
                @yield('container')
            <!-- .sign-up__container -->
        </div>
        <!-- .container -->
    </section>
    <!-- .sign-up -->
    <div class="modal-container">

        <div class="overlay_modal modal-trigger"></div>

        <div class="modal" role="dialog" aria-labelledby="modalTitle" aria-describedby="dialogDesc">
            <div class="popup_mine_title">
                <h3>Résultats</h3>
            </div>
            <div class="popup_mine_body">
                <div class="popup_mine_item">
                    <div class="box_note">
                        <p><span class="note">8</span><span class="separator">/</span>10</p>
                        <p> <strong>2022-2023</strong> </p>
                    </div>
                </div>
                <div class="popup_mine_item">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero corporis optio odio consequuntur?
                    Quam culpa nisi sequi? Corrupti architecto, qui quos nihil vel doloremque, iste dolorum a atque
                    voluptatum accusamus.
                </div>
            </div>
            <div class="popup_mine_footer">


                <div class="box_btn">
                    <a href="#" class="btn_mine modal-trigger" id="btn_close">Fermer</a>
                </div>
            </div>
        </div>

    </div>

    @include('partials.footer')
</body><!-- END BODY -->
<!-- Mirrored from weche.fpbenin.net/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 19 Jun 2023 12:20:49 GMT -->

</html>