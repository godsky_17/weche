@extends('base')
@section('container')
<div class="row containt mv-50">
    <div class="col-md-5 left ">
    <div class="row user-name container-mine">
            <div class="overlay">
                <p>{{ Auth::user()->categorie }}</p>
            </div>
            <h2 class="name"><span class="uppercase">{{ Auth::user()->nom }}</span> {{ Auth::user()->prenom }}</h2>
            <span class="matricule">{{ Auth::user()->matricule }}</span>
        </div>
        <div class="row container-mine">
            @if (!empty(Auth::user()->telephone) OR !empty(Auth::user()->email) )
            <div class="row info-box">
                <h4 class="title">Contact</h4>
                @if (!empty(Auth::user()->email))
                <p class="user-info"><span></span>{{ Auth::user()->email }}</p>
                @endif
                @if (!empty(Auth::user()->telephone))
                <p class="user-info">
                    <span></span>
                    {{Auth::user()->telephone}}
                </p>
                @endif

            </div>
            @endif

            <div class="row">
                @if (!empty($diplome))
                <div class="col-2 info-box">
                    <h4 class="title">Diplome(s)</h4>
                    <p class="user-info uppercase"><span></span>{{ $diplome }}</p>
                </div>
                @endif
                @if (!empty($distinction))
                <div class="col-3 info-box">
                    <h4 class="title">Distinction(s)</h4>
                    <p class="user-info"><span></span> {{ $distinction }}</p>
                </div>
                @endif


            </div>
            <div class="row info-box">
                @if (!empty($pension))
                <h4 class="title">Régime de retraite : <span class="user-info uppercase">{{ $pension }}</span></h4>
                @endif
                <h4 class="title">Année de service : <span class="user-info uppercase">5</span></h4>
                <h4 class="title">Service militaire : <span class="user-info uppercase">0</span></h4>
            </div>
        </div>

        <a href="{{ route('dashboard')}}">
            <div class="row container-mine btn_go_home">
                <h4 class="uppercase"> retour au dashborad</h4>
            </div>
        </a>

    </div>
    <div class="col-md-7 right container-mine">
        <div class="row graph">
            <h3>Mes performances</h3>
            <p>Ce graphe retrace votre évolution au cours des années.</p>
        </div>
        <div class="row resultats">
            <div class="row box_resultat">
                <div class="col-md-2 box_note">
                    <p><span class="note">8</span><span class="separator">/</span>10</p>
                    <p><strong>2022-2023</strong></p>
                </div>

                <div class="col-md-8">
                    Lorem ipsum dolor sit amet consectetur, adipisicing elit. Illum excepturi odit omnis
                    eligendi quae quisquam, error quos nulla quas aut enim. Dolore incidunt repellendus
                    veritatis eveniet facilis sapiente, quisquam iste.
                </div>
                <div class="col-md-2box_btn ">
                    <a href="#" class="modal-btn modal-trigger btn_mine">Détails</a>
                </div>
            </div>

            <div class="row box_resultat">
                <div class="col-md-2 box_note">
                    <p><span class="note">8</span><span class="separator">/</span>10</p>
                    <p><strong>2022-2023</strong></p>
                </div>

                <div class="col-md-8">
                    Lorem ipsum dolor sit amet consectetur, adipisicing elit. Illum excepturi odit omnis
                    eligendi quae quisquam, error quos nulla quas aut enim. Dolore incidunt repellendus
                    veritatis eveniet facilis sapiente, quisquam iste.
                </div>
                <div class="col-md-2box_btn ">
                    <a href="#" class="modal-btn modal-trigger btn_mine">Détails</a>
                </div>
            </div>

            <div class="row box_resultat">
                <div class="col-md-2 box_note">
                    <p><span class="note">8</span><span class="separator">/</span>10</p>
                    <p><strong>2022-2023</strong></p>
                </div>

                <div class="col-md-8">
                    Lorem ipsum dolor sit amet consectetur, adipisicing elit. Illum excepturi odit omnis
                    eligendi quae quisquam, error quos nulla quas aut enim. Dolore incidunt repellendus
                    veritatis eveniet facilis sapiente, quisquam iste.
                </div>
                <div class="col-md-2box_btn ">
                    <a hre="#" class="modal-btn modal-trigger btn_mine">Détails</a>
                </div>
            </div>



        </div>
    </div>
</div>
@endsection