@extends('base')
@section("container")

    <!-- <div class="uk-card uk-card-default uk-card-body uk-width-1-2@m">
        <div class="mb-4 text-sm text-gray-600">
            {{ __('Thanks for signing up! Before getting started, could you verify your email address by clicking on the link we just emailed to you? If you didn\'t receive the email, we will gladly send you another.') }}
        </div>

        @if (session('status') == 'verification-link-sent')
        <div class="mb-4 font-medium text-sm text-green-600">
            {{ __('A new verification link has been sent to the email address you provided during registration.') }}
        </div>
        @endif

        <div class="mt-4 flex items-center justify-between uk-margin-medium-top">
            <form method="POST" action="{{ route('verification.send') }}">
                @csrf

                <div class="uk-margin-small-right">
                    <x-primary-button class="uk-button uk-button-default">
                        {{ __('Resend Verification Email') }}
                    </x-primary-button>
                </div>
            </form>

            <form method="POST" action="{{ route('logout') }}">
                @csrf

                <button type="submit" class="uk-button uk-button-danger">
                    {{ __('Log Out') }}
                </button>
            </form>
        </div>
    </div> -->

    
    

    
<div class="uk-grid-collapse uk-child-width-expand@s uk-text-center uk-margin-large-top" uk-grid>
    <div>
        <img src="dist/img/message.jpg" alt="">
    </div>
    <div class="">
        <div class="mb-4 uk-text-default uk-text-normal uk-text-justify">
            {{ __('Merci de vous être inscrit ! Avant de commencer, pourriez-vous vérifier votre adresse électronique en
                 cliquant sur le lien que nous venons de vous envoyer ? Si vous n\'avez pas reçu l\'e-mail, nous vous en 
                 enverrons un autre avec plaisir.') }}
        </div>

        @if (session('status') == 'verification-link-sent')
        <div class="mb-4 uk-text-default uk-text-normal  uk-text-success uk-text-justify">
            {{ __('Un nouveau lien de vérification a été envoyé à l\'adresse électronique que vous avez fournie lors de votre inscription.') }}
        </div>
        @endif

        <div class="mt-4 flex items-center justify-between uk-margin-medium-top">
            <form method="POST" action="{{ route('verification.send') }}">
                @csrf

                <div class="uk-margin-small-right">
                    <x-primary-button class="uk-button uk-button-default">
                        {{ __('Renvoyer l\'e-mail de vérification') }}
                    </x-primary-button>
                </div>
            </form>

            <form method="POST" action="{{ route('logout') }}">
                @csrf

                <button type="submit" class="uk-button uk-button-danger">
                    {{ __('Déconnexion') }}
                </button>
            </form>
        </div>
    </div>
</div>


@endsection