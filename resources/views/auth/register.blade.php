@extends('base')
@section('container')

<div class="sign-up__container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2 col-xs-12">
            <div class="sign-up__header">
                <h1 class="sign-up__textcontent text-center">
                    <a href="#" class="sign-up__tab is-active">S'inscrire</a>
                </h1>
            </div>
            <!-- .sign-up__header -->
            <div class="col-md-6 col-md-offset-3 col-xs-12">
                
            </div>
            <div class="sign-up__main">

                <form action="{{ route('register') }}" method="post" class="sign-up__form sign-up__form--register is-visible" id="sign-up">
                    @csrf
                    <div class="row">
                        <div class="col-md-6 col-md-12">
                            <div class="form-group">
                                <label for="nom" class="sign-up__label">Nom <span class="text--red">*</span></label>
                                <input type="text"   class="sign-up__field" id="nom" name="nom" value="{{ old('nom') }}" placeholder="Votre nom de famille">
                                @error('nom')
                                    <p>Un problème avec le nom </p>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="prenom" class="sign-up__label">Prénom(s) <span class="text--red">*</span></label>
                                <input type="text" class="sign-up__field" id="prenom" name="prenom" value="{{ old('prenom') }}" placeholder="Vos prénoms">
                                @error('prenom')
                                    <p>Un problème avec le prenom </p>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="date_naiss" class="sign-up__label">Date de naissance <span class="text--red">*</span></label>
                                <input type="date" class="sign-up__field" id="date_naiss" name="date_naiss" value="{{ old('date_naiss') }}" placeholder="aaaa-mm-jj">
                                @error('date_naiss')
                                    <p>Un problème avec le date_naiss </p>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="matricule" class="sign-up__label">Numéro matricule <span class="text--red">*</span></label>
                                <input type="text" class="sign-up__field" id="matricule" name="matricule" value="{{ old('matricule') }}" placeholder="Votre numéro matricule">
                                @error('matricule')
                                    <p>Un problème avec le matricule </p>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 col-md-12">
                            <div class="form-group">
                                <label for="telephone" class="sign-up__label">Numéro de téléphone <span class="text--red">*</span></label>
                                <input type="text" class="sign-up__field" id="telephone" name="telephone" value="{{ old('telephone') }}" placeholder="Votre numéro de téléphone">
                                @error('telephone')
                                    <p>Un problème avec le telephone </p>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="email" class="sign-up__label">Email <span class="text--red">*</span></label>
                                <input type="email" class="sign-up__field" id="sign-up-email" name="email" value="{{ old('email') }}" placeholder="Votre adresse email" onkeyup="this.value = this.value.toLowerCase();">
                                @error('email')
                                    <p>Un problème avec le email </p>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="password" class="sign-up__label">Mot de passe <span class="text--red">*</span></label>
                                <input type="password" class="sign-up__field" id="password" name="password" placeholder="******">
                                @error('password')
                                    <p>Un problème avec le password </p>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="password_confirmation" class="sign-up__label">Confirmation du mot de
                                    passe <span class="text--red">*</span></label>
                                <input type="password" class="sign-up__field" id="password_confirmation" name="password_confirmation" placeholder="******">
                            </div>
                        </div>
                    </div>
                    <!--   <div class="sign-up__wrapper">
                                <input type="checkbox" for="sign-up-term" class="sign-up__checkbox">
                                <label for="sign-up-term" class="sign-up__desc">I agree all statements in <a href="#">Terms of Service</a></label>
                            </div>-->
                    <div class="form-actions">
                        <button type="submit" class="btn btn--blue">S'inscrire</button>
                    </div>
                            <div class="t-center">
                                <span>Vous avez déjà un compte? <a href="{{ route('login') }}">Inscrivez-vous</a></span>
                            </div>
                </form>
                <!-- .sign-up__form -->
            </div>
        </div>
    </div>
</div>

@endsection