@extends('base')
@section('container')
<div class="sign-up__container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2 col-xs-12">
            <div class="sign-up__header">
                <h1 class="sign-up__textcontent text-center">
                    <a href="#" class="sign-up__tab is-active">Se connecter</a>
                </h1>
            </div>
            <div class="sign-up__main">
                <form method="POST" action="{{ route('login') }}" class="sign-up__form is-visible" id="log-in">
                    @csrf
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3 col-xs-12">
                            <!-- Email Address -->
                            <div class="form-group">
                                <x-input-label for="email" :value="__('Email')" class="sign-up__label" />
                                <x-text-input id="email" class="sign-up__field" type="email" name="email" :value="old('email')" required autofocus autocomplete="username" />
                                <x-input-error :messages="$errors->get('email')" class="mt-2 mb-2" />
                            </div>

                            <!-- Password -->
                            <div class="mt-4 form-group">
                                <x-input-label for="password" :value="__('Password')" class="sign-up__label" />

                                <x-text-input id="password" class="sign-up__field" type="password" name="password" required autocomplete="current-password" />

                                <x-input-error :messages="$errors->get('password')" class="mt-2 mb-2" />
                            </div>

                            <!-- Remember Me -->
                            <div class="block mt-4 mb-20">
                                <label for="remember_me" class="inline-flex items-center">
                                    <input id="remember_me" type="checkbox" class="rounded border-gray-300 text-indigo-600 shadow-sm focus:ring-indigo-500" name="remember">
                                    <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
                                </label>
                            </div>

                            <div class="form-actions">
                                <button type="submit" class="btn btn--blue">Se connecter</button>
                                <a href="{{ route('password.request') }}" class="sign-up__link" style="margin-left: 10px;">Mot de passe oubli&eacute;?</a>
                            </div>
                            <div class="t-center">
                                <span>Vous n'avez pas de compte? <a href="{{ route('register') }}">Inscrivez-vous</a></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
            @endsection






            <!-- <div class="form-group">
                                <label for="log-in-email" class="sign-up__label">Identifiant <span class="text--red">*</span></label>
                                <input type="text" name="email" class="sign-up__field" id="log-in-email" placeholder="Email ou matricule" onkeyup="this.value = this.value.toLowerCase();">
                            </div>
                            <div class="form-group">
                                <label for="log-in-password" class="sign-up__label">Mot de passe <span class="text--red">*</span></label>
                                <input type="password" name="password" class="sign-up__field" id="log-in-password" placeholder="******">
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn--blue">Se connecter</button>
                                <a href="login/pass-reset.html" class="sign-up__link" style="margin-left: 10px;">Mot de passe oubli&eacute;?</a>
                            </div> -->