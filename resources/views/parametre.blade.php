@extends('base')
@section('container')
<div class="row containt mv-50">
    <div class="col-md-5 left ">
        <div class="row user-name container-mine">
            <h2 class="name uppercase">Paramètres</h2>
        </div>
        <div class="row container-mine p-20">
            <div class="menu-item param_hover active_param" id="menu1">
                <div class="text">
                    <p>Diplomes et distinctions</p>
                </div>
                <div class="img"><img src="img/93e12074377e36168b21a6ebc2d04cd5.jpg" alt="hello" srcset="" class="menu_img"></div>
            </div>

            <div class="menu-item param_hover" id="menu2">
                <div class="text">
                    <p>Famille</p>
                </div>
                <div class="img"><img src="img/93e12074377e36168b21a6ebc2d04cd5.jpg" alt="hello" srcset="" class="menu_img"></div>
            </div>

            <div class="menu-item param_hover" id="menu3">
                <div class="text">
                    <p>Informations</p>
                </div>
                <div class="img"><img src="img/93e12074377e36168b21a6ebc2d04cd5.jpg" alt="hello" srcset="" class="menu_img"></div>
            </div>

            <div class="menu-item param_hover" id="menu4">
                <div class="text">
                    <p>Services</p>
                </div>
                <div class="img"><img src="img/93e12074377e36168b21a6ebc2d04cd5.jpg" alt="hello" srcset="" class="menu_img"></div>
            </div>

        </div>
        <a href="{{ route('dashboard')}}">
            <div class="row container-mine btn_go_home">
                <h4 class="uppercase"> retour au dashborad</h4>
            </div>
        </a>

    </div>
    <div class="col-md-7 right container-mine">
        <div class="table">
            <div class="step active-step" id="step1">

                <div class="step_tite">
                    <h2>Diplomes et distinctions</h2>
                </div>
                <div class="form">
                    <form action="">
                        <h1>step1</h1>
                    </form>
                </div>
                <div class="control">
                    <input type="submit" value="Enregistrer" class="btn_submit">
                </div>

            </div>
            <div class="step" id="step2">
                <div class="step_tite">
                    <h2>Famille</h2>
                </div>
                <div class="form">
                    <form action="">
                        <h1>Step2</h1>
                    </form>
                </div>
                <div class="control">
                    <input type="submit" value="Enregistrer" class="btn_submit">
                </div>
            </div>
            <div class="step" id="step3">
                <div class="step_tite">
                    <h2>Informations</h2>
                </div>
                <div class="form">
                    <form action="">
                        <h1>Step3</h1>
                    </form>
                </div>
                <div class="control">
                    <input type="submit" value="Enregistrer" class="btn_submit">
                </div>
            </div>
            <div class="step" id="step4">
                <div class="step_tite">
                    <h2>Services</h2>
                </div>
                <div class="form">
                    <form action="">
                        <h1>step4</h1>
                    </form>
                </div>
                <div class="control">
                    <input type="submit" value="Enregistrer" class="btn_submit">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection