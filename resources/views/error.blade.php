@extends('base')
@section('container')
<div class="error-404__main">
    <div class="row">
        <div class="col-md-6">
            <img src="dist/realand/images/uploads/404.jpg" alt="404">
        </div><!-- error-404__img -->

        <div class="col-md-6">
            <h1 class="error-404__title">Whoops!</h1>
            <div class="error-404__detail">
                <h2 class="error-404__explain">Une erreur 404 est survenue.</h2>
                <p class="error-404__desc">La page que vous avez demandé ne peut être trouvée. Retournez à la page d'accueil.</p>
            </div><!-- .error-404__detail -->

            <a href="/" class="error-404__cta">Accueil</a>
        </div><!-- .error-404__content -->
    </div><!-- .row -->
</div>
@endsection