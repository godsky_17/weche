@extends('base')
@section('container')

<div class="sign-up__container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2 col-xs-12">
            <div class="sign-up__header">
                <h1 class="sign-up__textcontent text-center">
                    <a href="#log-in" class="sign-up__tab is-active">Se connecter</a>
                    <span>ou</span>
                    <a href="#sign-up" class="sign-up__tab ">S'inscrire</a>
                </h1>
            </div>
            <!-- .sign-up__header -->
            <div class="col-md-6 col-md-offset-3 col-xs-12">
                
            </div>
            <div class="sign-up__main">
                <form action="{{ route('login') }}" method="post" class="sign-up__form is-visible" id="log-in">
                    @csrf
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3 col-xs-12">
                            <div class="form-group">
                                <label for="log-in-email" class="sign-up__label">Identifiant <span class="text--red">*</span></label>
                                <input type="text" name="email" class="sign-up__field" id="log-in-email" placeholder="Email ou matricule" onkeyup="this.value = this.value.toLowerCase();">
                            </div>
                            <div class="form-group">
                                <label for="log-in-password" class="sign-up__label">Mot de passe <span class="text--red">*</span></label>
                                <input type="password" name="password" class="sign-up__field" id="log-in-password" placeholder="******">
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn--blue">Se connecter</button>
                                <a href="login/pass-reset.html" class="sign-up__link" style="margin-left: 10px;">Mot de passe oubli&eacute;?</a>
                            </div>
                        </div>
                    </div>
                </form><!-- .sign-up__form -->

                <form action="{{ route('register') }}" method="post" class="sign-up__form sign-up__form--register " id="sign-up">
                    @csrf
                    <div class="row">
                        <div class="col-md-6 col-md-12">
                            <div class="form-group">
                                <label for="nom" class="sign-up__label">Nom <span class="text--red">*</span></label>
                                <input type="text" class="sign-up__field" id="nom" name="nom" placeholder="Votre nom de famille">
                            </div>
                            <div class="form-group">
                                <label for="prenom" class="sign-up__label">Prénom(s) <span class="text--red">*</span></label>
                                <input type="text" class="sign-up__field" id="prenom" name="prenom" placeholder="Vos prénoms">
                            </div>
                            <div class="form-group">
                                <label for="date_naiss" class="sign-up__label">Date de naissance <span class="text--red">*</span></label>
                                <input type="date" class="sign-up__field" id="date_naiss" name="date_naiss" placeholder="aaaa-mm-jj">
                            </div>
                            <div class="form-group">
                                <label for="matricule" class="sign-up__label">Numéro matricule <span class="text--red">*</span></label>
                                <input type="text" class="sign-up__field" id="matricule" name="matricule" placeholder="Votre numéro matricule">
                            </div>
                        </div>
                        <div class="col-md-6 col-md-12">
                            <div class="form-group">
                                <label for="phone" class="sign-up__label">Numéro de téléphone <span class="text--red">*</span></label>
                                <input type="text" class="sign-up__field" id="phone" name="telephone" placeholder="Votre numéro de téléphone">
                            </div>
                            <div class="form-group">
                                <label for="email" class="sign-up__label">Email <span class="text--red">*</span></label>
                                <input type="email" class="sign-up__field" id="sign-up-email" name="email" placeholder="Votre adresse email" onkeyup="this.value = this.value.toLowerCase();">
                            </div>
                            <div class="form-group">
                                <label for="password" class="sign-up__label">Mot de passe <span class="text--red">*</span></label>
                                <input type="password" class="sign-up__field" id="password" name="password" placeholder="******">
                            </div>
                            <div class="form-group">
                                <label for="password_confirm" class="sign-up__label">Confirmation du mot de
                                    passe <span class="text--red">*</span></label>
                                <input type="password" class="sign-up__field" id="password_confirm" name="password_confirmation" placeholder="******">
                            </div>
                        </div>
                    </div>
                    <!--   <div class="sign-up__wrapper">
                                <input type="checkbox" for="sign-up-term" class="sign-up__checkbox">
                                <label for="sign-up-term" class="sign-up__desc">I agree all statements in <a href="#">Terms of Service</a></label>
                            </div>-->
                    <div class="form-actions">
                        <button type="submit" class="btn btn--blue">S'inscrire</button>
                    </div>
                </form>
                <!-- .sign-up__form -->
            </div>
        </div>
    </div>
</div>

@endsection