<!DOCTYPE html>
<html lang="fr"><!-- BEGIN HEAD -->
<!-- Mirrored from weche.fpbenin.net/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 19 Jun 2023 12:20:37 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->

<head>
    <meta charset="utf-8" />
    <title>Weche</title>
    <meta name="viewport" content="width&#x3D;device-width,&#x20;user-scalable&#x3D;no,&#x20;initial-scale&#x3D;1.0,&#x20;minimum-scale&#x3D;1.0,&#x20;maximum-scale&#x3D;1.0">
    <meta name="description" content="">
    <meta name="author" content="Gbemiga&#x20;OGOUBY">
    <meta name="keywords" content="">
    <meta name="format-detection" content="telephone-no"> <!-- Mobile specific meta -->
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="dist/front_assets/webfont/webfont.html"></script>
    <script>
        WebFont.load({
            google: {
                "families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
            },
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!-- BEGIN HEAD CSS -->
    <link href="dist/img/fav.png" rel="shortcut&#x20;icon">
    <link href="https://fonts.googleapis.com/css?family=Open Sans:400,300,600,700&amp;subset=all" media="screen" rel="stylesheet" type="text&#x2F;css">

    
    <link href="dist/realand/css/plugins.css" media="screen" rel="stylesheet" type="text&#x2F;css">
    <link href="dist/realand/css/style.css" media="screen" rel="stylesheet" type="text&#x2F;css">
    <link href="dist/metro_2/simple-line-icons/simple-line-icons.min.css" media="screen" rel="stylesheet" type="text&#x2F;css">
    <link href="dist/custom_plugins/font-awesome/css/font-awesome.min.css" media="screen" rel="stylesheet" type="text&#x2F;css">
    <link href="dist/custom_plugins/bootstrap-sweetalert/sweet-alert.css" media="screen" rel="stylesheet" type="text&#x2F;css">
    <link href="dist/custom_plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" media="screen" rel="stylesheet" type="text&#x2F;css">
    <link href="dist/custom_plugins/select2_alt/css/select2.css" media="screen" rel="stylesheet" type="text&#x2F;css">
    <link href="dist/custom_plugins/select2_alt/css/select2-bootstrap.min.css" media="screen" rel="stylesheet" type="text&#x2F;css">
    <link href="dist/css/custom.css" media="screen" rel="stylesheet" type="text&#x2F;css"> 
    <link rel="stylesheet" href="dist/css/style.css"><!-- END HEAD CSS-->
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <!--   <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>       <!-- END GLOBAL MANDATORY STYLES -->
    <script type="text&#x2F;javascript" src="dist/js/login/index.js"></script>
    <script type="text&#x2F;javascript" src="dist/custom_plugins/jquery.min.js"></script>
    <script type="text&#x2F;javascript" src="dist/custom_plugins/jquery-migrate.min.js"></script>
    <link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css'>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css'>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.5/font/bootstrap-icons.css">
    <link rel="stylesheet" href="dist/dist/style.css">
</head>
<body>
<div class="container">
    <div class="close_mine">
        <a href="#">
            <i class="bi bi-x-circle"></i>
        </a>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="page-ath-wrap">
                <div class="page-ath-content register-form-content">
                    <div class="page-ath-form">
                        <div class="form-align-box">
                            <div class="wizard">
                                <!-- <div class="progress" style="height: 30px;">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="4" style="width: 25%;">
                                        Step 1 of 4
                                    </div>
                                </div> -->
                                <form role="form" action="{{ route('save') }}" method="post" class="register-wizard-box">
                                    @csrf
                                    <div class="tab-content" id="main_form">
                                        <div class="tab-pane active" role="tabpanel" id="step1">
                                            <div class="form-input-steps">
                                                <h4>Completer vos informations</h4>
                                                <h3>Informations personnelles</h3>
                                                <div class="row">
                                                    <span id="error"></span>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="nom" class="sign-up__label">Nom <span class="text--red">*</span></label>
                                                            <input type="text" class="sign-up__field" id="nom" name="nom"  placeholder="Votre nom de famille" value="{{ Auth::user()->nom }}"  required>
                                                            @if($errors->has('nom'))
                                                                <p>Le champ « nom » a une erreur</p>
                                                            @endif
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="prenom" class="sign-up__label">Prénom(s) <span class="text--red">*</span></label>
                                                            <input type="text" class="sign-up__field" id="prenom"  name="prenom" placeholder="Vos prénoms" value="{{ Auth::user()->prenom }}">
                                                            @if($errors->has('prenom'))
                                                                <p>Le champ « prenom » a une erreur</p>
                                                            @endif
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="date_naiss" class="sign-up__label">Date de naissance <span class="text--red">*</span></label>
                                                            <input type="date" class="sign-up__field" id="date_naiss" name="date_naiss" placeholder="aaaa-mm-jj"  value="{{Auth::user()->date_naiss}}">
                                                            @if($errors->has('date_naiss'))
                                                                <p>Le champ « date_naiss » a une erreur</p>
                                                            @endif
                                                            
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="lieu_naiss" class="sign-up__label">Lieu de naissance <span class="text--red">*</span></label>
                                                            <select name="lieu_naiss" id="lieu_naiss" class="sign-up__field" value="{{ old('lieu_naiss') }}">
                                                                <option value="Cotonou">Cotonou</option>
                                                                <option value="Calavi">Calavi</option>
                                                                <option value="Parakou">Parakou</option>
                                                                <option value="Porto-Novo">Porto-Novo</option>
                                                                <option value="Abomey">Abomey</option>
                                                                <option value="Savalou">Savalou</option>
                                                            </select>
                                                            @if($errors->has('lieu_naiss'))
                                                                <p>Le champ « lieu_naiss » a une erreur</p>
                                                            @endif
                                                            
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="telephone" class="sign-up__label">Numéro de téléphone <span class="text--red">*</span></label>
                                                            <input type="text" class="sign-up__field" id="telephone" name="telephone" placeholder="Votre numéro de téléphone" value="{{ Auth::user()->telephone }}">
                                                            @if($errors->has('telephone'))
                                                                <p>Le champ « telephone » a une erreur</p>
                                                            @endif
                                                            
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="matricule" class="sign-up__label">Numéro matricule <span class="text--red">*</span></label>
                                                            <input type="text" class="sign-up__field" id="matricule" name="matricule" placeholder="Votre numéro matricule" value="{{ Auth::user()->matricule }}">
                                                            @if($errors->has('matricule'))
                                                                <p>Le champ « matricule » a une erreur</p>
                                                            @endif
                                                                                                                        
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="email" class="sign-up__label">Email <span class="text--red">*</span></label>
                                                            <input type="email" class="sign-up__field" id="email" name="email" placeholder="Votre adresse email" onkeyup="this.value = this.value.toLowerCase();" value="{{ Auth::user()->email }}">
                                                            @if($errors->has('email'))
                                                                <p>Le champ « email » a une erreur</p>
                                                            @endif
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="profession" class="sign-up__label">Votre profession avant d'etre enroller dans la fonction publique ?</label>
                                                            <select name="profession" id="profession" class="sign-up__field" value="{{ old('profession') }}">
                                                                <option value="Cotonou">Cotonou</option>
                                                                <option value="Calavi">Calavi</option>
                                                                <option value="Parakou">Parakou</option>
                                                                <option value="Porto-Novo">Porto-Novo</option>
                                                                <option value="Abomey">Abomey</option>
                                                                <option value="Savalou">Savalou</option>
                                                            </select>
                                                            @if($errors->has('profession'))
                                                                <p>Le champ « profession » a une erreur</p>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <ul class="list-inline text-right">
                                                <li>
                                                    <button type="button" class="step-btn prev-step"><i class="fa fa-chevron-left"></i><span>Back</span></button>
                                                </li>
                                                <li>
                                                    <button type="button" class="step-btn next-step step2" id="essaie"><span>Next</span><i class="fa fa-chevron-right"></i></button>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="tab-pane" role="tabpanel" id="step2">
                                            <div class="form-input-steps addZone1">
                                                <h4>Completer vos informations</h4>
                                                <h3>Information supplémentaire</h3>
                                                <div class="row"><span class="error"></span></div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="date_nomination" class="sign-up__label">Date de nomination à un emploi rétribué par l'administration <span class="text--red">*</span></label>
                                                            <input type="date" class="sign-up__field" id="date_nomination" name="date_nomination" placeholder="aaaa-mm-jj">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="grade" class="sign-up__label">Categorie de recrutement<span class="text--red">*</span></label>
                                                            <select name="grade" id="grade" class="sign-up__field" value="{{ old('grade') }}">
                                                                @foreach ($categories as $item)
                                                                    <option value="{{ $item->id }}">{{ $item->nom}}</option>    
                                                                @endforeach
                                                            </select>
                                                            @if($errors->has('grade'))
                                                                <p>Le champ « grade » a une erreur</p>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="regime_pension" class="sign-up__label">Régime des pensions<span class="text--red">*</span></label>
                                                            <select name="regime_pension" id="regime_pension" class="sign-up__field" value="{{ old('regime_pension') }}">
                                                                @foreach ($pensions as $item)
                                                                    <option value="{{ $item->id}}"> {{ $item->nom }}</option>
                                                                @endforeach
                                                            </select>
                                                            @if($errors->has('regime_pension'))
                                                                <p>Le champ « regime_pension » a une erreur</p>
                                                            @endif
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="grade_actuelle" class="sign-up__label">Categorie actuelle<span class="text--red">*</span></label>
                                                            <select name="grade_actuelle" id="grade_actuelle" class="sign-up__field" value="{{ old('grade_actuelle') }}">
                                                                @foreach ($categories as $item)
                                                                    <option value="{{ $item->id }}">{{ $item->nom }}</option>    
                                                                @endforeach
                                                            </select>
                                                            @if($errors->has('grade_actuelle'))
                                                                <p>Le champ « grade_actuelle » a une erreur</p>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <label for="" class="sign-up__label">Avez vous eu des interruptions de service ?</label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="date_interruption_debut" class="sign-up__label">Date de début</label>
                                                            <input type="date" class="sign-up__field" id="date_interruption_debut" name="date_interruption_debut[]" placeholder="aaaa-mm-jj">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="date_interruption_fin" class="sign-up__label">Date de fin</label>
                                                            <input type="date" class="sign-up__field" id="date_interruption_fin" name=" date_interruption_fin[]" placeholder="aaaa-mm-jj" >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 form-group"">
                                                        <label for=" cause_interruption" class="sign-up__label">Cause</label>
                                                        <textarea class="contact-form__field contact-form__comment form-control" cols="30" rows="2" placeholder="Message" name="cause_interruption[]" id="cause_interruption"></textarea>
                                                    </div>
                                                            @if($errors->has('cause_interruption'))
                                                                <p>Le champ « cause_interruption » a une erreur</p>
                                                            @endif
                                                </div>
                                            </div>

                                            <div class="row">
                                                <a type="button" class="header__cta justify-content-md-end addinterruption" id="addinterruption">Ajouter une interruption</a>
                                            </div>
                                            <ul class="list-inline text-right">
                                                <li>
                                                    <button type="button" class="step-btn prev-step"><i class="fa fa-chevron-left"></i><span>Back</span></button>
                                                </li>
                                                <li>
                                                    <button type="button" class="step-btn next-step step3"><span>Next</span><i class="fa fa-chevron-right"></i></button>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="tab-pane" role="tabpanel" id="step3">
                                            <h4>Completer vos informations</h4>
                                            <h3>Mutation, nomination & promotion</h3>
                                            <div class="row"><span class="error"></span></div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="date_mutation" class="sign-up__label">Date de votre dèrnière mutation</label>
                                                        <input type="date" class="sign-up__field" id="date_mutation" name="date_mutation" placeholder="aaaa-mm-jj" >
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="date_nomination_second" class="sign-up__label">Date de votre dèrnière nomination à un poste</label>
                                                        <input type="date" class="sign-up__field" id="date_nomination_second" name="date_nomination_second" placeholder="aaaa-mm-jj">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="date_promotion" class="sign-up__label">Date de promotion</label>
                                                        <input type="date" class="sign-up__field" id="date_promotion" name="date_promotion" placeholder="aaaa-mm-jj" >
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="num_arrete_mutation" class="sign-up__label">Numéro de l'arreté (Mutation)</label>
                                                        <input type="text" class="sign-up__field" id="num_arrete_mutation" name="num_arrete_mutation" placeholder="Votre numéro de téléphone" value="{{ old('num_arrete_mutation') }}">
                                                        
                                                        @if($errors->has('num_arrete_mutation'))
                                                                <p>Le champ « num_arrete_mutation » a une erreur</p>
                                                            @endif
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="num_arrete_nomination" class="sign-up__label">Numéro de l'arreté (Nomination)</label>
                                                        <input type="text" class="sign-up__field" id="num_arrete_nomination" name="num_arrete_nomination" placeholder="Votre numéro de téléphone" value="{{ old('num_arrete_nomination') }}">
                                                        @if($errors->has('num_arrete_nomination'))
                                                                <p>Le champ « num_arrete_nomination » a une erreur</p>
                                                            @endif
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="new_grade" class="sign-up__label">Nouveau grade </label>
                                                        <select name="new_grade" id="new_grade" class="sign-up__field" value="{{ old('new_grade') }}">
                                                            @foreach ($categories as $item)
                                                                <option value="{{ $item->id }}">{{ $item->nom}}</option>    
                                                            @endforeach
                                                        </select>
                                                        @if($errors->has('new_grade'))
                                                                <p>Le champ « new_grade » a une erreur</p>
                                                            @endif
                                                    </div>
                                                </div>


                                            </div>
                                            <ul class="list-inline text-right">
                                                <li>
                                                    <button type="button" class="step-btn prev-step"><i class="fa fa-chevron-left"></i><span>Back</span></button>
                                                </li>
                                                <li>
                                                    <button type="button" class="step-btn next-step step4"><span>Next</span><i class="fa fa-chevron-right"></i></button>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="tab-pane" role="tabpanel" id="step4">
                                            <h4>Completer vos informations</h4>
                                            <h3>Services, diplome & distinction</h3>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="diplome" class="sign-up__label">Quelle est votre diplome ? <span class="text--red">*</span> </label>
                                                        <select name="diplome" id="diplome" class="sign-up__field" value="{{ old('diplome') }}">
                                                            @foreach ($diplomes as $item)
                                                                <option value="{{ $item->nom }}">{{ $item->nom }}</option>
                                                            @endforeach
                                                        </select>
                                                        @if($errors->has('diplome'))
                                                                <p>Le champ « diplome » a une erreur</p>
                                                            @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-6">

                                                    <div class="form-group">
                                                        <label for="distinction" class="sign-up__label">Distinction honorifique</label>
                                                        <select name="distinction" id="distinction" class="sign-up__field" value="{{ old('distinction') }}">
                                                            @foreach ($distinctions as $item)
                                                                <option value="{{ $item->id }}">{{ $item->nom }}</option>
                                                            @endforeach
                                                        </select>
                                                        @if($errors->has('distinction'))
                                                                <p>Le champ « distinction » a une erreur</p>
                                                            @endif
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label for="" class="sign-up__label">Vos états de services militaires</label>
                                                </div>
                                            </div>
                                            <div class="addZone2">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <label for="debut_service_militaire" class="sign-up__label">Début</label>
                                                        <input type="date" class="sign-up__field" id="debut_service_militaire" name="debut_service_militaire[]" placeholder="aaaa-mm-jj">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for="fin_service_militaire" class="sign-up__label">Fin</label>
                                                        <input type="date" class="sign-up__field" id="fin_service_militaire" name="fin_service_militaire[]" placeholder="aaaa-mm-jj" >
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="row">
                                                <a type="button" class="header__cta justify-content-md-end addservice">Ajouter un service militaire</a>
                                            </div>
                                            <ul class="list-inline text-right">
                                                <li>
                                                    <button type="button" class="step-btn prev-step"><i class="fa fa-chevron-left"></i><span>Back</span></button>
                                                </li>
                                                <li>
                                                    <button type="button" class="step-btn next-step step4"><span>Next</span><i class="fa fa-chevron-right"></i></button>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="tab-pane" role="tabpanel" id="step5">
                                            <div class="form-input-steps add_zone">
                                                <h4>Completer vos informations</h4>
                                                <h3>Votre famille</h3>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="date_mariage" class="sign-up__label">Date de mariage</label>
                                                            <input type="date" class="sign-up__field" id="date_mariage" name="date_mariage" placeholder="aaaa-mm-jj" value="{{ old('date_mariage') }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="dialecte" class="sign-up__label">Dialecte parlé</label>
                                                            <select name="dialecte" id="dialecte" class="sign-up__field" value="{{ old('dialecte') }}">
                                                                @foreach ($dialectes as $item)
                                                                    <option value="{{ $item->id }}">{{ $item->nom }}</option>
                                                                @endforeach
                                                            </select>
                                                        @if($errors->has('dialecte'))
                                                                <p>Le champ « dialecte » a une erreur</p>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="nom_enfant" class="sign-up__label">Nom de votre enfant</label>
                                                            <input type="text" class="sign-up__field" id="nom_enfant" name="nom_enfant[]" placeholder="Votre nom de famille">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="date_naiss_enfant" class="sign-up__label">Date de naissance de votre enfant</label>
                                                            <input type="date" class="sign-up__field" id="date_naiss_enfant" name="date_naiss_enfant[]" placeholder="aaaa-mm-jj" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row ">
                                                <a type="button" class="header__cta justify-content-md-end add">Ajouter un enfant</a>
                                            </div>
                                            <ul class="list-inline text-right">
                                                <li>
                                                    <button type="button" class="step-btn prev-step"><i class="fa fa-chevron-left"></i><span>Back</span></button>
                                                </li>
                                                <li>
                                                    <button type="submit" class="step-btn next-step"><span>Finish</span><i class="fa fa-chevron-right"></i></button>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </form>
                                <div class="wizard-inner">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation">
                                            <a href="#step1" class="active" data-toggle="tab" aria-controls="step1" role="tab" aria-expanded="true" data-step="1">
                                                <span class="round-tab">1</span>
                                            </a>
                                        </li>
                                        <li role="presentation">
                                            <a href="#step2" class="disabled" data-toggle="tab" aria-controls="step2" role="tab" aria-expanded="false" data-step="2">
                                                <span class="round-tab">2</span>
                                            </a>
                                        </li>
                                        <li role="presentation">
                                            <a href="#step3" class="disabled" data-toggle="tab" aria-controls="step3" role="tab" data-step="3">
                                                <span class="round-tab">3</span>
                                            </a>
                                        </li>
                                        <li role="presentation">
                                            <a href="#step4" class="disabled" data-toggle="tab" aria-controls="step4" role="tab" data-step="4">
                                                <span class="round-tab">4</span>
                                            </a>
                                        </li>
                                        <li role="presentation">
                                            <a href="#step5" class="disabled" data-toggle="tab" aria-controls="step5" role="tab" data-step="5">
                                                <span class="round-tab">5</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src='https://code.jquery.com/jquery-3.4.1.min.js'></script>
<script src='https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js'></script>
<script src='https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js'></script>
<script src="dist/dist/script.js"></script>
<script src="dist/realand/js/control.js"></script>
</body>
</html>


