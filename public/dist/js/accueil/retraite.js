/**
 * Created by FREEBY on 03/07/2017.
 */
$(document).ready(function(){
    $('.select2').select2()

    jQuery.validator.addMethod("phoneCk", function (phone_number, element) {
        phone_number = phone_number.replace(/\s+/g, "");
        return this.optional(element) || phone_number.length > 7 && phone_number.match(/^([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})?([0-9]{2})?([0-9]{2})$/);
    }, 'Numéro de téléphone incorrect.');

    jQuery.validator.addMethod('positiveNumber',
        function (value) {
            return Number(value) > 0;
        }, 'Numéro matricule incorrect.');

    jQuery.validator.addMethod("notEqual", function(value, element, param) {
        return this.optional(element) || value != param;
    }, "Le contact d'urgence doit être différent de votre numéro de téléphone");

    FormRetraite();
});

var FormRetraite = function () {
    $('form#retraite_form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                matricule: {
                    required: true,
                    minlength:4,
                    positiveNumber: true
                },
                telephone: {
                    required: true,
                    phoneCk: true
                },
                contact_urgence: {
                    required: true,
                    phoneCk: true,
                    notEqual: '#telephone'
                }
            },

            messages: {
            },
            invalidHandler: function (event, validator) { //display error alert on form submit

            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            errorPlacement: function (error, element) {
                if (element.closest('.select2').size() === 1) {
                    error.insertAfter(element.closest('.err-container'));
                } else {
                    error.insertAfter(element.closest('.form-control'));
                }
            },
            submitHandler: function (form) {
                    form.submit()
            }

        }
    );
};