/**
 * Created by skyfall_thezone on 18/05/2016.
 */
$(document).ready(function () {
    $(document).find('.show_title').tooltip();
    $(document).find('.select2').select2();

    $('.doc_detail').click(function(){
        var doc = $(this).attr('data-id')
        $.blockUI({
            message: 'Patientez...',
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            } });
        $.ajax({
            url: '/home/getDossier',
            type: 'POST',
            dataType: 'json',
            data: {
                id_dossier: doc
            },
            success: function (response) {
                var  pieces = ['an','pps', 'app', 'dip', 'cvr', 'ctar', 'etp', 'are'];
                $.each(pieces, function(k,v){
                    var span = $('span#'+v);
                    span.html(response[v]);
                    span.removeClass('green');
                    span.removeClass('red');
                    if(response[v]=='OUI')
                    {
                        span.addClass('green')
                    }
                    else{
                        span.addClass('red')
                    }
                    $('td#'+v+'_obs').html(response[v+'_obs']);
                });
                $.unblockUI();
                $('#piece-modal').modal('show');
            },
            error: function(){
                $.unblockUI();
            }
        });

    });
});


var msg_box = function (mes_aff, type) {
    $.bootstrapGrowl(mes_aff,{
        ele: 'div.page-content', // which element to append to
        type: type, // (null, 'info', 'danger', 'success', 'warning')
        offset: {
            from: 'top',
            amount: 50
        }, // 'top', or 'bottom'
        align: 'center', // ('left', 'right', or 'center')
        width: 'auto', // (integer, or 'auto')
        delay: 2000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
        allow_dismiss: false, // If true then will display a cross to close the popup.
        stackup_spacing: 10 // spacing between consecutively stacked growls.
    });
};

function getBaseURL() {
    var url = location.href;
    var baseURL = url.substring(0, url.indexOf('/', 14));

    if (baseURL.indexOf('http://localhost') != -1) {
        var pathname = location.pathname;
        var index1 = url.indexOf(pathname);
        var index2 = url.indexOf("/", index1 + 1);
        var baseLocalUrl = url.substr(0, index2);

        return baseLocalUrl;
    }
    else {
        return baseURL;
    }

}
