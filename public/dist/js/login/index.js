/**
 * Created by skyfall_thezone on 12/04/2017.
 */
jQuery(document).ready(function(){

    jQuery.validator.addMethod("phoneCk", function (phone_number, element) {
        phone_number = phone_number.replace(/\s+/g, "");
        return this.optional(element) || phone_number.length > 7 && phone_number.match(/^([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})?([0-9]{2})?([0-9]{2})$/);
    });

    $.validator.addMethod('matriculeCk', function (value) {
        return /^[0-9]+$/.test(value);
    }, 'Matricule incorrect.');

    $('#sign-up-email').bind('change', function(){
        $(this).val(function(_, v){
            return v.replace(/\s+/g, '');
        });
    });

    SignUpForm();
    LoginForm();
});


var SignUpForm = function () {
    $('form#sign-up').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                //generale
                nom: {
                    required: true
                },
                prenom: {
                    required: true
                },
                matricule: {
                    required: true,
                    minlength: 3,
                    matriculeCk: true
                },
                email_ins: {
                    required: true,
                    email:true
                },
                telephone: {
                    required: true,
                    minlength: 8,
                    maxlength: 8,
                    phoneCk: true
                },
                date_naiss: {
                    required: true
                },
                password: {
                    required: true,
                    minlength: 6
                },
                password_confirmation: {
                    required: true,
                    equalTo: '#password'
                }
            },

            messages: {

            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            }
            ,

            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            errorPlacement: function (error, element) {
                if (element.attr("name") == "tnc") { // insert checkbox errors after the container
                    error.insertAfter($('#register_tnc_error'));
                } else if (element.closest('.input-icon').size() === 1) {
                    error.insertAfter(element.closest('.input-icon'));
                } else {
                    error.insertAfter(element);
                }
            },submitHandler: function (form) {
               form.submit()
            }
    });
};

var LoginForm = function () {
    $('form#log-in').validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            //generale
            username: {
                required: true
            },
            password: {
                required: true,
                minlength: 6
            }
        },

        messages: {

        },
        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        }
        ,

        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
            label.remove();
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "tnc") { // insert checkbox errors after the container
                error.insertAfter($('#register_tnc_error'));
            } else if (element.closest('.input-icon').size() === 1) {
                error.insertAfter(element.closest('.input-icon'));
            } else {
                error.insertAfter(element);
            }
        },submitHandler: function (form) {
            form.submit()
        }
    });
};