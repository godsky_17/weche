const modalContainer = document.querySelector(".modal-container");
const modalTriggers = document.querySelectorAll(".modal-trigger");


modalTriggers.forEach(trigger => trigger.addEventListener("click", toggleModal))

function toggleModal(){
    modalContainer.classList.toggle("active")
}


// parametre
function moveActive() {

    const activeMenu = document.querySelector('.active_param')
    const activeStep = document.querySelector('.active-step')
    activeMenu.classList.remove('active_param')
    activeStep.classList.remove('active-step')
    
}


const menu1 = document.getElementById('menu1')
const menu2 = document.getElementById('menu2')
const menu3 = document.getElementById('menu3')
const menu4 = document.getElementById('menu4')

const step1 = document.getElementById('step1')
const step2 = document.getElementById('step2')
const step3 = document.getElementById('step3')
const step4 = document.getElementById('step4')

menu1.addEventListener('click', function step1page() {
    moveActive()

    menu1.classList.add('active_param')
    step1.classList.add('active-step')
})


menu2.addEventListener('click', function step2page() {
        moveActive()

        menu2.classList.add('active_param')
        step2.classList.add('active-step')
})


menu3.addEventListener('click', function step3page() {
    moveActive()
    
    menu3.classList.add('active_param')
    step3.classList.add('active-step')
})

menu4.addEventListener('click', function step4page() {
    moveActive()
    
    menu4.classList.add('active_param')
    step4.classList.add('active-step')
})

// affichage du sous menu
function showMenu(){
    const show = document.querySelector('.subMenu')
    if (show.classList != 'subMenu show') {
        show.classList.add('show')    
    }else{
        show.classList.remove('show') 
    }
    
}
const subMenu = document.querySelector('#subMenu')
subMenu.addEventListener('click', showMenu)