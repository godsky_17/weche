const addBtn = document.querySelector('.add');
const addZone = document.querySelector('.add_zone');

/**
 * cette fonction permet de dupliquer le groupe de champs qui permet de récuppérer 
 * les informations en rapport avec les enfants à la page 3 du formulaire d'inscription
 */
function addInput() {
    const newZone = document.createElement('div')
    newZone.classList.add('row')
    const rand = Math.floor(Math.random() * 101)
    newZone.id = rand
    newZone.innerHTML = ` <div class="col-md-6">

                            <div class="form-group">
                                <label for="date_mariage" class="sign-up__label">Nom de votre enfant<span class="text--red">*</span></label>
                                <input type="text" class="sign-up__field" id="nom" name="nom" placeholder="Votre nom de famille" required>
                            </div>    
                        </div>
                        <div class="col-md-6">
                        <div class="del">
                         <button type="button" class="delete" onclick=deleteZone(${rand})>X</button>
                        </div>
                            <div class="form-group">
                            <label for="phone" class="sign-up__label">Date de naissance de votre enfant<span class="text--red">*</span></label>
                            <input type="date" class="sign-up__field" id="date_naiss_enfant" name="date_naiss_enfant" placeholder="aaaa-mm-jj">
                        </div>
                        </div>
                        `
    addZone.appendChild(newZone)

}


function deleteZone(id) {
    const zone = document.getElementById(id);
    zone.remove();
}
addBtn.addEventListener('click', addInput);


const addBtn1 = document.querySelector('.addinterruption');
const addZone1 = document.querySelector('.addZone1');

/**
 * cette fonction permet de dupliquer le groupe de champs qui permet de récuppérer 
 * les informations en rapport avec interruption de service
 */
function addInput1() {
    const newZone = document.createElement('div')
    newZone.classList.add('row')
    const rand = Math.floor(Math.random() * 101)
    newZone.id = rand
    newZone.innerHTML = `   <div class="col-md-6">
                                <div class="form-group">
                                    <label for="date_interruption_debut" class="sign-up__label">Date de début</label>
                                    <input type="date" class="sign-up__field" id="date_interruption_debut" name="date_interruption_debut[]" placeholder="aaaa-mm-jj">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="del">
                                    <button type="button" class="delete" onclick=deleteZone1(${rand})>X</button>
                                </div>
                                <div class="form-group">
                                    <label for="date_interruption_fin" class="sign-up__label">Date de fin</label>
                                    <input type="date" class="sign-up__field" id="date_interruption_fin"" name="date_interruption_fin[]" placeholder="aaaa-mm-jj">
                                </div>
                            </div>
                            <div class="col-md-12 form-group"">
                                <label for=" date_interruption_fin" class="sign-up__label">Cause</label>
                                <textarea class="contact-form__field contact-form__comment form-control" cols="30" rows="2" placeholder="Message" name="cause_interruption[]" required></textarea>
                            </div>
                        `
    addZone1.appendChild(newZone)

}


function deleteZone1(id) {
    const zone = document.getElementById(id);
    zone.remove();
}
addBtn1.addEventListener('click', addInput1);

const addBtn2 = document.querySelector('.addservice');
const addZone2 = document.querySelector('.addZone2');

/**
 * cette fonction permet de dupliquer le groupe de champs qui permet de récuppérer 
 * les informations en rapport avec les services militaire
 */
function addInput2() {
    const newZone = document.createElement('div')
    newZone.classList.add('row')
    const rand = Math.floor(Math.random() * 101)
    newZone.id = rand
    newZone.innerHTML = `
                        <div class="col-md-6">
                            <label for="phone" class="sign-up__label">Début</label>
                            <input type="date" class="sign-up__field" id="date_mariage" name="date_mariage" placeholder="aaaa-mm-jj">
                        </div>
                        <div class="col-md-6">
                            <div class="del">
                            <button type="button" class="delete" onclick=deleteZone1(${rand})>X</button>
                            </div>
                                <label for="phone" class="sign-up__label">Fin</label>
                                <input type="date" class="sign-up__field" id="date_mariage" name="date_mariage" placeholder="aaaa-mm-jj">
                        </div>
                        `
    addZone2.appendChild(newZone)

}


function deleteZone2(id) {
    const zone = document.getElementById(id);
    zone.remove();
}
addBtn2.addEventListener('click', addInput2);

/* control du formulaire */
const nex1btn = document.getElementById('essaie')
nex1btn.addEventListener('click', function(e){
    // console.log('bonjour')
    // alert('salut')
})
