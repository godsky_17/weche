function validateStep1() {
    const nom = document.getElementById('nom')
    const prenom = document.getElementById('prenom')
    const date_naiss = document.getElementById('date_naiss')
    const lieu_naiss = document.getElementById('lieu_naiss')
    const phone = document.getElementById('telephone')
    const matricule = document.getElementById('matricule')
    const email = document.getElementById('email')
    const profession = document.getElementById('profession')

    if ((nom.value.trim() != '') && (lieu_naiss.value.trim() != '') && (date_naiss.value != '') &&
        (phone.value.trim() != '') && (matricule.value.trim() != '') && (email.value.trim() != '')) {
        var $active = $('.wizard .nav-tabs li a.active');
        $active.parent().next().children().removeClass('disabled');
        $active.parent().addClass('done');
        nextTab($active);
       

        const errorMode = document.getElementById('error')
        errorMode.classList.remove('error')
        errorMode.innerHTML = ""

    } else {
        const errorMode = document.getElementById('error')
        errorMode.classList.add('error')
        errorMode.innerHTML = "Vous devez remplire les champs obligatoire"
    }
}

function validateStep2() {
    const date = document.getElementById('date_nomination')
    const grade = document.getElementById('grade')
    const regime = document.getElementById('regime_pension')
    const grad = document.getElementById('grade_actuelle')

    if ((date.value.trim() != '') && (grade.value.trim() != '') && (regime.value.trim() != '') && (grad.value.trim() != '')) {
        
        var $active = $('.wizard .nav-tabs li a.active');
        $active.parent().next().children().removeClass('disabled');
        $active.parent().addClass('done');
        nextTab($active);
       
        const errorMode = document.getElementById('error')
        errorMode.classList.remove('error')
        errorMode.innerHTML = ""
    } else {
        const errorMode = document.getElementById('error')
        errorMode.classList.add('error')
        errorMode.innerHTML = "Vous devez remplire les champs obligatoire"       
    }
}

function validateStep3(){
    // const date_mutation = document.getElementById('date_mutation')
    // const date_nomination = document.getElementById('date_nomination')
    // const date_promotion = document.getElementById('date_promotion')
    // const num_arrete_mutation = document.getElementById('num_arrete_mutation')
    // const new_grade = document.getElementById('new_grade')
    // const num_arrete_nomination = document.getElementById('num_arrete_nomination')

        var $active = $('.wizard .nav-tabs li a.active');
        $active.parent().next().children().removeClass('disabled');
        $active.parent().addClass('done');
        nextTab($active);

}

function validateStep4(){
    const diplome = document.getElementById('diplome')
    if (diplome.value.trim() != '') {
        var $active = $('.wizard .nav-tabs li a.active');
        $active.parent().next().children().removeClass('disabled');
        $active.parent().addClass('done');
        nextTab($active);
    }
        

}
// ------------register-steps--------------
$(document).ready(function () {
    $('.nav-tabs > li a[title]').tooltip();
    //Wizard
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var $target = $(e.target);
        if ($target.hasClass('disabled')) {
            return false;
        }
      
        // handle with prgressbar 
        var step = $(e.target).data('step');
        var percent = (parseInt(step) / 4) * 100;
        $('.progress-bar').css({ width: percent + '%' });
        $('.progress-bar').text('Step ' + step + ' of 4');
      
    });

    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
        var $target = $(e.target);
        $target.parent().addClass('active');
    });

    $('a[data-toggle="tab"]').on('hide.bs.tab', function (e) {
        var $target = $(e.target);
        $target.parent().removeClass('active');
    });


    $(".step2").click(function (e) {
        validateStep1()       
    });

    $(".step3").click(function (e) {
        validateStep2()       
    });

    $(".step4").click(function (e) {
        validateStep3()       
    });

    $(".step5").click(function (e) {
        validateStep4()       
    });
    $(".prev-step").click(function (e) {
        var $active = $('.wizard .nav-tabs li a.active');
        prevTab($active);
    });
});

function nextTab(elem) {
    $(elem).parent().next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
    $(elem).parent().prev().find('a[data-toggle="tab"]').click();
}























































// // ------------register-steps--------------
// $(document).ready(function () {
//     $('.nav-tabs > li a[title]').tooltip();
//     //Wizard
//     $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
//         var $target = $(e.target);
//         if ($target.hasClass('disabled')) {
//             return false;
//         }
      
//         // handle with prgressbar 
//         var step = $(e.target).data('step');
//         var percent = (parseInt(step) / 4) * 100;
//         $('.progress-bar').css({ width: percent + '%' });
//         $('.progress-bar').text('Step ' + step + ' of 4');
      
//     });

//     $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
//         var $target = $(e.target);
//         $target.parent().addClass('active');
//     });

//     $('a[data-toggle="tab"]').on('hide.bs.tab', function (e) {
//         var $target = $(e.target);
//         $target.parent().removeClass('active');
//     });


//     $(".next-step").click(function (e) {
//         const go = false
//         if(go){
//             var $active = $('.wizard .nav-tabs li a.active');
//             $active.parent().next().children().removeClass('disabled');
//             $active.parent().addClass('done');
//             nextTab($active);
//         }else{
//             alert('Je n\'ai pas le droit de faire ça')
//         }
        
//     });

//     $(".prev-step").click(function (e) {
//         var $active = $('.wizard .nav-tabs li a.active');
//         prevTab($active);
//     });
// });

// function nextTab(elem) {
//     $(elem).parent().next().find('a[data-toggle="tab"]').click();
// }
// function prevTab(elem) {
//     $(elem).parent().prev().find('a[data-toggle="tab"]').click();
// }