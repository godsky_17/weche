<?php

namespace Database\Seeders;

use App\Models\Listedistinction;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ListedistinctionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        listedistinction::factory()->create([
            'nom' => 'Ordre national du Bénin'
        ]);

        listedistinction::factory()->create([
            'nom' => 'Ordre du Mérite du Bénin'
        ]);

        listedistinction::factory()->create([
            'nom' => 'Ordre du Mérite Agricole'
        ]);


        listedistinction::factory()->create([
            'nom' => 'Ordre du Mérite du Travail'
        ]);

        Listedistinction::factory()->create([
            'nom' => 'Ordre des Palmes Académiques'
        ]);

        listedistinction::factory()->create([
            'nom' => 'Ordre du Mérite Maritime'
        ]);

        listedistinction::factory()->create([
            'nom' => 'Ordre du Mérite de la Santé'
        ]);


        listedistinction::factory()->create([
            'nom' => 'Médaille de Reconnaissance'
        ]);
    }
}
