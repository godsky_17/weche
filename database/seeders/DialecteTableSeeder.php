<?php

namespace Database\Seeders;

use App\Models\Dialecte;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DialecteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        Dialecte::factory()->create([
            'nom' => 'Fon'
        ]);

        Dialecte::factory()->create([
            'nom' => 'Yoruba'
        ]);

        Dialecte::factory()->create([
            'nom' => 'Bariba'
        ]);

        Dialecte::factory()->create([
            'nom' => 'Dendi'
        ]);

        Dialecte::factory()->create([
            'nom' => 'Ditammari'
        ]);

        Dialecte::factory()->create([
            'nom' => 'Goun'
        ]);

        Dialecte::factory()->create([
            'nom' => 'Adja'
        ]);

        Dialecte::factory()->create([
            'nom' => 'Mahi'
        ]);

        Dialecte::factory()->create([
            'nom' => 'Ife'
        ]);

        Dialecte::factory()->create([
            'nom' => 'Aizo'
        ]);

        Dialecte::factory()->create([
            'nom' => 'Nagot'
        ]);

        Dialecte::factory()->create([
            'nom' => 'Lokpa'
        ]);

        Dialecte::factory()->create([
            'nom' => 'Wama'
        ]);

        Dialecte::factory()->create([
            'nom' => 'Mina'
        ]);

        Dialecte::factory()->create([
            'nom' => 'Natimba'
        ]);

        Dialecte::factory()->create([
            'nom' => 'Betammaribe'
        ]);

        Dialecte::factory()->create([
            'nom' => 'Peul'
        ]);

        Dialecte::factory()->create([
            'nom' => 'Sahouè'
        ]);

        Dialecte::factory()->create([
            'nom' => 'Holli'
        ]);
    }
}
