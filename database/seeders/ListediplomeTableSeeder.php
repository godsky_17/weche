<?php

namespace Database\Seeders;

use App\Models\Listediplome;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ListediplomeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        listediplome::factory()->create([
            'nom' =>'Baccalauréat'
        ]);

        listediplome::factory()->create([
            'nom' =>'Licence '
        ]);

        listediplome::factory()->create([
            'nom' =>'Baccalauréat'
        ]);

        listediplome::factory()->create([
            'nom' =>'BEPC'
        ]);

        listediplome::factory()->create([
            'nom' =>'DEA'
        ]);

        listediplome::factory()->create([
            'nom' =>'DESS'
        ]);

        listediplome::factory()->create([
            'nom' =>'Doctorat'
        ]);


        listediplome::factory()->create([
            'nom' =>'Diplôme d\'Ingénieur'
        ]);

        listediplome::factory()->create([
            'nom' =>'DUT'
        ]);

        listediplome::factory()->create([
            'nom' =>'CAP'
        ]);


        listediplome::factory()->create([
            'nom' =>'BTS'
        ]);
        

        Listediplome::factory()->create([
            'nom' =>'DTS'
        ]);

        listediplome::factory()->create([
            'nom' =>'CQP'
        ]);

        listediplome::factory()->create([
            'nom' =>'DEUST'
        ]);
    }
}
