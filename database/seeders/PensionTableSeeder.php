<?php

namespace Database\Seeders;

use App\Models\Pension;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PensionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        Pension::factory()->create([
            'nom' => 'CNSS'
        ]);

        Pension::factory()->create([
            'nom' => 'CN'
        ]);

        Pension::factory()->create([
            'nom' => 'RAF'
        ]);
    }
}
