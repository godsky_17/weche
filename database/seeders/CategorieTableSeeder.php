<?php

namespace Database\Seeders;

use App\Models\Categorie;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategorieTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        Categorie::factory()->create([
            'nom'=>'A1'
        ]);

        Categorie::factory()->create([
            'nom'=>'A2'
        ]);

        Categorie::factory()->create([
            'nom'=>'A3'
        ]);


        Categorie::factory()->create([
            'nom'=>'B1'
        ]);

        Categorie::factory()->create([
            'nom'=>'B2'
        ]);

        Categorie::factory()->create([
            'nom'=>'B3'
        ]);


        Categorie::factory()->create([
            'nom'=>'C1'
        ]);

        Categorie::factory()->create([
            'nom'=>'C2'
        ]);

        Categorie::factory()->create([
            'nom'=>'C3'
        ]);


        Categorie::factory()->create([
            'nom'=>'D1'
        ]);

        Categorie::factory()->create([
            'nom'=>'D2'
        ]);

        Categorie::factory()->create([
            'nom'=>'D3'
        ]);

        

        Categorie::factory()->create([
            'nom'=>'E1'
        ]);

        Categorie::factory()->create([
            'nom'=>'E2'
        ]);

        Categorie::factory()->create([
            'nom'=>'E3'
        ]);

        Categorie::factory()->create([
            'nom'=>'F1'
        ]);

        Categorie::factory()->create([
            'nom'=>'F2'
        ]);

        Categorie::factory()->create([
            'nom'=>'F3'
        ]);
    }
}
