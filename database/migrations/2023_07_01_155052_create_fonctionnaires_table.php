<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('fonctionnaires', function (Blueprint $table) {
            $table->id();
            $table->string('matricule')->unique();
            $table->string('nom');
            $table->string('prenom');
            $table->string('email')->unique();
            $table->integer('telephone')->nullable()->unique();
            $table->date('date_naiss')->nullable();
            $table->string('lieu_naiss')->nullable();
            $table->date('nomination')->nullable();
            $table->string('password')->unique();
            $table->string('profession')->nullable();
            $table->date('date_mariage')->nullable();
            $table->string('wizard');
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->foreignId('regime_id')->constrained('pensions')->nullable();
            $table->foreignId('categorie_id')->constrained('categories')->nullable();
            $table->foreignId('role_id')->constrained('roles')->nullable();
            $table->foreignId('dialecte_id')->constrained('dialectes')->nullable();
            $table->foreignId('distinction_id')->constrained('listedistinctions')->nullable();
            $table->timestamps();
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {

        // regime_id
        Schema::table('fonctionnaires', function(Blueprint $table){
            $table->dropConstrainedForeignId('regime_id');
        });

        // categorie_id
        Schema::table('fonctionnaires', function(Blueprint $table){
            $table->dropConstrainedForeignId('categorie_id');
        });

        //role_id
        Schema::table('fonctionnaires', function(Blueprint $table){
            $table->dropConstrainedForeignId('role_id');
        });

        //dialecte_id
        Schema::table('fonctionnaires', function(Blueprint $table){
            $table->dropConstrainedForeignId('dialecte_id');
        });

        //distinction_id
        Schema::table('fonctionnaires', function(Blueprint $table){
            $table->dropConstrainedForeignId('distinction_id');
        });

        Schema::dropIfExists('fonctionnaires');
    }
};
