<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('interruptions', function (Blueprint $table) {
            $table->id();          
            $table->date('debut')->nullable();
            $table->date('fin')->nullable();
            $table->string('cause')->nullable();
            $table->foreignId('fonctionnaire_id')->constrained('fonctionnaires');
            $table->timestamps();
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('interruptions', function(Blueprint $table){
            $table->dropConstrainedForeignId('fonctionnaire_id');
        });
        Schema::dropIfExists('interruptions');
    }
};
