<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('evaluations', function (Blueprint $table) {
            $table->id();
            $table->float('note');
            $table->integer('annee');
            $table->string('observation')->nullable();
            $table->foreignId('fonctionnaire_id')->constrained('fonctionnaires');
            $table->timestamps();
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('evaluations', function(Blueprint $table){
            $table->dropConstrainedForeignId('fonctionnaire_id');
        });

        Schema::dropIfExists('evaluations');
    }
};
