<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Smillitaire extends Model
{
    use HasFactory;

    protected $fillable = [
        'debut',
        'fin',
        'total',
        'fonctionnaire_id'
    ];
}
