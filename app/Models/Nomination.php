<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Nomination extends Model
{
    use HasFactory;

    protected $fillable = [
        'date',
        'nouveau_poste',
        'ancien_poste',
        'reference',
        'fonctionnaire_id'
    ];
}
