<?php

namespace App\Http\Controllers;

use App\Models\Pension;
use App\Models\Dialecte;
use App\Models\Categorie;
use App\Models\Diplome;
use App\Models\Enfant;
use App\Models\Listediplome;
use Illuminate\Http\Request;
use App\Models\Fonctionnaire;
use App\Models\Interruption;
use Ramsey\Uuid\Type\Integer;
use App\Models\Listedistinction;
use App\Models\Mutation;
use App\Models\Nomination;
use App\Models\Promotion;
use App\Models\Smillitaire;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class FonctionnaireController extends Controller
{
    //
public function dashboard(){
        $fonctionnaire = Fonctionnaire::where('id', '=', Auth::user()->id)->get();
        
        foreach (Categorie::where('id', '=', Auth::user()->id)->get() as $value) {
            $categorie =  $value->nom;
        };

        foreach (Pension::where('id', '=', Auth::user()->id)->get() as $value) {
            $pension =  $value->nom;
        };

        foreach (Dialecte::where('id', '=', Auth::user()->id)->get() as $value) {
            $dialecte =  $value->nom;
        };

        foreach (Listedistinction::where('id', '=', Auth::user()->id)->get() as $value) {
            $distinction =  $value->nom;
        };

        foreach (Listediplome::where('id', '=', Auth::user()->id)->get() as $value) {
            $diplome =  $value->nom;
        };


        // dd(view('dashboard', compact('fonctionnaire','categorie','pension','dialecte','distinction','diplome')));
        return view('dashboard', compact('fonctionnaire','categorie','pension','dialecte','distinction',"diplome"));
}

    public function resultat(){
        $fonctionnaire = Fonctionnaire::where('id', '=', Auth::user()->id)->get();
        
        foreach (Categorie::where('id', '=', Auth::user()->id)->get() as $value) {
            $categorie =  $value->nom;
        };

        foreach (Pension::where('id', '=', Auth::user()->id)->get() as $value) {
            $pension =  $value->nom;
        };

        foreach (Dialecte::where('id', '=', Auth::user()->id)->get() as $value) {
            $dialecte =  $value->nom;
        };

        foreach (Listedistinction::where('id', '=', Auth::user()->id)->get() as $value) {
            $distinction =  $value->nom;
        };

        foreach (Listediplome::where('id', '=', Auth::user()->id)->get() as $value) {
            $diplome =  $value->nom;
        };

        return view('resultat', compact('fonctionnaire','categorie','pension','dialecte','distinction',"diplome"));

    }

    public function parametre(){
        return view('parametre');
    }

    public function wizard()
    {
        //je retourne la page d'inscription
        $categories = Categorie::all();
        $pensions = Pension::all();
        $diplomes = Listediplome::all();
        $distinctions = Listedistinction::all();
        $dialectes = Dialecte::all();
        return view('wizard', compact('categories', 'pensions','diplomes', 'distinctions', 'dialectes'));
    }

    public function save(Request $request){

        $request -> validate([
            'nom' => ['required', 'string', 'max:255'],
            'prenom' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'telephone' => ['required', 'string', 'min:8', 'max:12'],
            'date_naiss' => ['required', 'date', 'before:2005/12/31'],
            'lieu_naiss' => ['required', 'string' , 'max:255'],
            'date_nomination' => ['required' , 'date'],
            
            'profession' => ['string', 'max:225'],
            // 'date_mariage' => ['date'],
            'regime_pension' => ['required', 'integer', 'max:5'],
            'grade' => ['required', 'integer', 'max:5'],
            'matricule' => ['required', 'string', 'max:15'],
            'grade_actuelle' => ['integer', 'max:5'],
            // 'date_interruption_debut' => ['date'],
            // 'date_interruption_fin' => ['date'],

            // 'cause_interruption' => ['string', 'max:1000'],
            // 'date_mutation' => ['date'],
            // 'date_nomination_second' => ['date'],
            // 'date_promotion' => ['date'],
            // 'num_arrete_mutation' => ['string'],
            // 'num_arrete_nomination' => ['string'],
            'new_grade'  => ['integer', 'min:0', 'max:5'],
            'diplome'  => ['required', 'string', 'max:255'],
            // 'distinction'  => ['string'],
            // 'debut_service_militaire' => ['date'],
            // 'fin_service_militaire' => ['date'],
            // 'date_mariage' => ['date'],
            'dialecte'  => ['required', 'integer', 'max:5'],
            // 'nom_enfant'  => ['string', 'max:255', 'min:0'],
            // 'date_naiss_enfant' => ['required','date'],
        ]);

        // dd($request);
        if (!empty($request->diplome)) {
            $diplome = Diplome::create([
                'nom' => $request->diplome,
                'fonctionnaire_id' => Auth::user()->id
                
            ]);
        }
        if (!empty($request->date_nomination_second) && !empty($request->num_arrete_nomination)) {
            $diplome = Nomination::create([
                'date' => $request->date_nomination_second,
                'reference' => $request->num_arrete_nomination,
                'fonctionnaire_id' => Auth::user()->id
                
            ]);
        }

        

        if (!empty($request->date_mutation) && !empty($request->num_arrete_mutation)) {
            $mutaion = Mutation::create([
                'date' => $request->date_mutation,
                'reference' => $request->num_arrete_mutation,
                'fonctionnaire_id' => Auth::user()->id
                
            ]);
        }

        if (!empty($request->date_promotion) && !empty($request->new_grade)) {
            $promotion = Promotion::create([
                'date' => $request->date_promotion,
                'grade' => $request->new_grade,
                'fonctionnaire_id' => Auth::user()->id
                
            ]);
        }
        

        $fonctionnaire = DB::table('fonctionnaires')->where('id','=', Auth::user()->id)->update([
            'matricule' => $request->matricule,
            'nom' => $request->nom,
            'prenom' => $request->prenom,
            'email' => $request->email,
            'telephone' => $request->telephone,
            'date_naiss' => $request->date_naiss,
            'lieu_naiss' => $request->lieu_naiss,
            'profession' => $request->profession,
            'nomination' => $request->date_nomination,
            'date_mariage' => $request->date_mariage,
            'wizard' => 1,
            'regime_id' => $request->regime_pension,
            'categorie_id' => $request->grade,
            'dialecte_id' => $request->dialecte,
            
        ]);
        $this->saveInterruption($request->date_interruption_debut,$request->date_interruption_fin,$request->cause_interruption);
        $this->saveSmilitaire($request->debut_service_militaire,$request->fin_service_militaire);
        $this->saveEnfant($request->nom_enfant,$request->date_naiss_enfant);

       return redirect('/dashboard');
    }



    function saveInterruption($debut,$fin,$cause){
        if (!empty($debut) && !empty($fin) && !empty($cause)){

            for ($i=0; $i < count($debut) ; $i++) { 
                $out = Interruption::create(
                    [
                        'debut' => $debut[$i], 
                        'fin' => $fin[$i], 
                        'cause' => $cause[$i], 
                        'fonctionnaire_id' => Auth::user()->id
                    ]
                );
            }
            return $out;
        }

        
    }

    function saveSmilitaire($debut,$fin){
        if (!empty($debut) && !empty($fin)){

            for ($i=0; $i < count($debut) ; $i++) { 
                $out = Smillitaire::create(
                    [
                        'debut' => $debut[$i], 
                        'fin' => $fin[$i],  
                        'fonctionnaire_id' => Auth::user()->id
                    ]
                );
            }
            return $out;
        }    
    }

    function saveEnfant($nom,$date){
        if (!empty($nom) OR !empty($date)){

            for ($i=0; $i < count($nom) ; $i++) { 
                $out = Enfant::create(
                    [
                        'nom' => $nom[$i], 
                        'date_naiss' => $date[$i],  
                        'fonctionnaire_id' => Auth::user()->id
                    ]
                );
            }
            return $out;
        }    
    }

}
