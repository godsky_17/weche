<?php

use App\Http\Controllers\FonctionnaireController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('inscription-connexion');
});

Route::get('/dashboard', [FonctionnaireController::class, 'dashboard'])->middleware(['auth', 'verified'])->name('dashboard');

Route::post('/login-users', [LoginController::class, 'authenticate'])->name('dologin');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';

Route::get('/base', function(){
    return view('base');
});


Route::get('/faq', function(){
    return view('aide');
})->name('faq');

Route::get('/contact', function(){
    return view('contact');
})->name('contact');


Route::get('/error', function(){
    return view('error');
})->name('error');

Route::get('/resultats', [FonctionnaireController::class, 'resultat'])->middleware('auth')->name('fonctionnaire.resultat');
Route::get('/parametre', [FonctionnaireController::class, 'parametre'])->middleware('auth')->name('fonctionnaire.parametre');
Route::get('/bienvenue',  [FonctionnaireController::class, 'wizard'])->middleware('auth')->name('wizard');
Route::post('/bienvenue',  [FonctionnaireController::class, 'save'])->middleware('auth')->name('save');


